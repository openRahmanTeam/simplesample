package com.simple.sample;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.simple.sample.entity.ChildData;
import com.simple.sample.entity.MasterData;
import com.simple.sample.service.ChildDataService;
import com.simple.sample.service.MasterDataService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleSampleApplicationTests {
	
	@Autowired
	private MasterDataService masterService;
	
	@Autowired
	private ChildDataService childService;
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	MasterData master = new MasterData();
	
	private MasterData setMaster() throws ParseException{
		
		
		master.setCreateTime(dateFormat.parse("01-01-2017"));
		master.setDescription("master data description");
		master.setStatus(true);
		master.setUpdateBy("maman");
		master.setUpdateTime(new Date());
		return master;
	}

//	@Test
	public void contextLoads() throws ParseException {
		
		masterService.save(setMaster());
	}
	
//	@Test
	public void testDelete(){
		master.setId("9f8466ab-b9e3-4d9d-913a-711a3ff5ebc3"); 
		masterService.delete(master);
	}
	
	@Test
	public void testChild() throws ParseException{
		ChildData c = new ChildData();
		master=new MasterData();
		master.setId("275d9763-4db3-4f38-a872-0b13842d5d6e");
		
		c.setCreateTime(dateFormat.parse("01-01-2017"));
		c.setDescription("child data desc");
		c.setMasterData(master);
		c.setQty(12);
		c.setStatus(true);
		c.setUpdateBy("mamann");
		childService.save(c);
	}

}
