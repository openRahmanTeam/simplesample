package com.simple.sample.controller;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simple.sample.entity.ChildData;
import com.simple.sample.service.ChildDataService;

@RestController
@RequestMapping("/api/child")
public class ChildDataController extends GenericController<ChildDataService, ChildData> {

	@Override
	protected void getId(ChildData c, String id) {
		c.setId(id);
	}

	@Override
	protected void setDefault(ChildData c) {
		c.setUpdateTime(new Date());
	}

	
}
