package com.simple.sample.controller;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simple.sample.entity.MasterData;
import com.simple.sample.service.MasterDataService;

@RestController
@RequestMapping("/api/master")
public class MasterDataController extends GenericController<MasterDataService, MasterData> {

	@Override
	protected void getId(MasterData m, String id) {
		m.setId(id);
	}

	@Override
	protected void setDefault(MasterData m) {
		m.setUpdateTime(new Date());
	}
}
