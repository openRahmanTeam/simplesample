package com.simple.sample.controller;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

public abstract class GenericController<S extends CrudRepository<E, String>, E> {

	@Autowired
	private S service;
	
	ResponseEntity<String> result = null;
	JSONObject json = new JSONObject();
	
	@RequestMapping(method=RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Iterable<E> listAll(){
		return service.findAll();
	}
	
	@RequestMapping(value="id={id}", method=RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<E> getById(@PathVariable("id") String id){
		E e = service.findOne(id);
		if(e == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(e, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.ACCEPTED)
	public String save(@RequestBody E m){
		String value = null;
		try {
			setDefault(m);
			service.save(m);
			value = "data has been saved";
		} catch (Exception e) {
			value = "Data failed to save";
			e.printStackTrace();
		}
		return value;
	}
	
	@RequestMapping(value="id={id}",method=RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String> delete(@PathVariable("id") String id) throws JSONException{
		try {
			if(service.findOne(id)==null){
				json.put("info", "empty data");
				json.put("message", "ID does not exist");
				result = new ResponseEntity<>(json.toString(), HttpStatus.NOT_FOUND);
			} else {
				service.delete(id);
				json.put("info", "deleting data");
				json.put("message", "Data has been deleted");
				result = new ResponseEntity<>(json.toString(), HttpStatus.OK);
			}
		} catch (Exception e) {
			json.put("info", "Data used by other transaction");
			json.put("message", "Data does not deleted");
			result = new ResponseEntity<String>(json.toString(), HttpStatus.CONFLICT);
			e.printStackTrace();
		}
		return result;
	}
	
	protected abstract void getId(E m, String id);
	protected abstract void setDefault(E m);
	
	@RequestMapping(value="id={id}", method=RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public String update(@RequestBody E m, @PathVariable("id") String id){
		String result =  null;
		try {
			if(service.findOne(id) == null){
				result = "id does not exist";
			} else {
				getId(m, id);
				setDefault(m);
				//m.setUpdateTime(new Date());
				service.save(m);
				result =  "Data has been updated";
			}
		} catch (Exception e) {
			result = "Data failed to update";
		}
		
		return result;
	}
}
