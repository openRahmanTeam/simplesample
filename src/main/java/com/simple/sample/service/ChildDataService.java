package com.simple.sample.service;

import org.springframework.data.repository.CrudRepository;

import com.simple.sample.entity.ChildData;

public interface ChildDataService extends CrudRepository<ChildData, String>{

}
