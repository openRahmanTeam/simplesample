package com.simple.sample.service;

import org.springframework.data.repository.CrudRepository;

import com.simple.sample.entity.MasterData;

public interface MasterDataService extends CrudRepository<MasterData, String>{

}
